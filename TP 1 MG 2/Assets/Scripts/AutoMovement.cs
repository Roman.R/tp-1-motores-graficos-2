using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMovement : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float leftBoundary = -5f; // L�mite izquierdo
    public float rightBoundary = 5f; // L�mite derecho

    private int direction = 1;

    void Update()
    {
        // Calcular la nueva posici�n
        Vector3 newPosition = transform.position + new Vector3(direction * moveSpeed * Time.deltaTime, 0f, 0f);

        // Verificar si se alcanz� alguno de los l�mites
        if (newPosition.x < leftBoundary)
        {
            direction = 1; // Cambiar a la direcci�n derecha
        }
        else if (newPosition.x > rightBoundary)
        {
            direction = -1; // Cambiar a la direcci�n izquierda
        }

        // Mover el objeto a la nueva posici�n
        transform.position = newPosition;
    }
}

